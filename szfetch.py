#!/usr/bin/env python3

import os
import re
import sys
import glob
import signal
import urllib
import logging
import requests
import argparse
import threading
import subprocess
import bs4
from bs4 import BeautifulSoup
from datetime import datetime
from colorlog import ColoredFormatter
from concurrent.futures import ThreadPoolExecutor

log = logging.getLogger('pythonConfig')
page_url_pool = None
exec_fileio = None
pageid_map = dict()
stats_total_pages = 0
args = None


class PagePool:
    page_urls = set()
    cv = None
    all_pages_added = False
    poison_stop = False
    pool_already_used = False

    def __init__(self):
        self.cv = threading.Condition()

    def get(self):
        ret = None
        if not self.pool_already_used:
            self.pool_already_used = True
        with self.cv:
            while len(self.page_urls) == 0:
                # atomic release & block until wakeup
                if self.all_pages_added or self.poison_stop:
                    return None
                self.cv.wait(timeout=1.0)
            ret = self.page_urls.pop()
        if self.poison_stop:
            ret = None
        return ret

    def add(self, url):
        with self.cv:
            self.page_urls.add(url)
            self.cv.notify()

    def set_all_pages_added(self):
        self.all_pages_added = True

    def set_poison_stop(self):
        self.poison_stop = True

    def is_already_used(self):
        return self.pool_already_used


def signal_handler_poison(signal, frame):
    # set poison_stop in page_url_pool
    log.info('SIGINT: controlled ending of szfetch initiated')
    page_url_pool.set_poison_stop()
    # hard exit only if not downloads have been started already
    # otherwise: finish download
    if not page_url_pool.is_already_used():
        sys.exit(0)


def get_content(url):
    "return a website's content"
    content = None
    headers = { 'User-Agent': '''Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) \
               AppleWebKit/537.36 (KHTML, like Gecko) \
               Chrome/35.0.1916.47 Safari/537.36''' }
    req = urllib.request.Request(url, data=None, headers=headers)

    try:
        f = urllib.request.urlopen(req)
    except (urllib.error.HTTPError, urllib.error.URLError) as e:
        log.error('[get_content] {0}: {1}'.format(url, e))
    else:
        content = f.read()
        f.close()
    return content


def libnet_login():
    "post libnetlogin & create server session; returns base_url or None"
    payload = {'j_username': 'performIpLogin', 'j_password': '',
               'ipLoginApplication' : 'LIBNET', 'loginType': ''}
    sec_check_url='https://archiv.szarchiv.de/Portal/j_security_check'
    base_url = None

    # create request-Session for serverside session-management
    s = requests.Session()
    # POST with form-encoded data
    sp = s.post(sec_check_url, payload)
    if sp.status_code != 200 or sp.content != b'':
        return None

    sg = s.get('https://archiv.szarchiv.de/Portal/restricted/Start.act')
    soup = BeautifulSoup(sg.content, features="html.parser")
    for link in soup.find_all('a'):
        target = link.attrs.get('target', None)
        if target == 'diz_pdfNavigation':
            base_url = link.attrs.get('href', None)
    log.debug('base_url: {0}'.format(base_url))
    return base_url


def scrape_hrefs(url):
    "scrape set of hrefs of site (url)"
    site_content = get_content(url)
    hrefs = set()
    soup = BeautifulSoup(site_content, features="html.parser")

    for link in soup.find_all('a'):
        if 'href' in link.attrs:
            hrefs.add(link.attrs['href'])
    return hrefs


def get_pageid_mappings(url):
    "get pageid mappings (to page) for url"
    site_content = get_content(url)
    tree = BeautifulSoup(site_content, features="html.parser")
    pagesp_node = tree.find(attrs={'class': re.compile(r'PagesP')})
    for node in pagesp_node.findAll(attrs={'class': re.compile(r'S[0-9]+')}):
        for el in node.find('span').contents:
            el = str(el).strip()
            # can't test whether page nr is < 100, bc there
            # are some weird page numbers like 801 etc.
            if el.isdigit():
                pageid_map[node['class'][0]] = el


def scrape_iframe_src(url):
    "scrape the single iframe src of site (url)"
    page = get_content(url)
    soup = BeautifulSoup(page, features="html.parser")
    return soup.find("iframe").get("src")


def scrape_day_urls(szversion, year_url, date_pattern=None):
    "scrape set of full day urls from year-site (year_url)"
    year_hrefs = scrape_hrefs(year_url)
    days_all_lnks = set()
    pattern = re.compile(r'hh03.*SZ\.'+szversion+'\.[0-9]{8}\..*$')

    for href in year_hrefs:
        if pattern.match(href):
            if date_pattern and date_pattern in href:
                days_all_lnks.add('https://service3.szarchiv.de/hh03/' + href)
            if not date_pattern:
                days_all_lnks.add('https://service3.szarchiv.de/hh03/' + href)
    return sorted(days_all_lnks)


def scrape_year_url(szversion, base_url, year):
    "scrape the year url from the base site"
    base_hrefs = scrape_hrefs(base_url)
    pattern = re.compile(r'hh03.*SZ\.' + szversion + r'\.[0-9]{4}\..*$')
    for href in base_hrefs:
        if pattern.match(href) and year in href:
                return 'https://service3.szarchiv.de/hh03/' + href


def extract_date_from_url(page_url):
    "extract date from page url"
    date_pattern = re.compile(r'.*bid=SZ([0-9]+).*')
    return date_pattern.search(page_url).group(1)


def extact_pageid_from_url(page_url):
    "extract pageid from page url"
    page_pattern = re.compile(r'.*bid=SZ[0-9]+S([0-9]+).*')
    return page_pattern.search(page_url).group(1)


def rm_watermark(file_path):
    "start the exif-/metadata-removing script for a pdf-file"
    DEVNULL = open(os.devnull, 'w')
    script_path = './rm_watermark.sh'
    subprocess.check_call([script_path, file_path], stdout=DEVNULL, stderr=DEVNULL)
    DEVNULL.close()


def fileio_worker(filename, content):
    "write downloaded pdfs to file and kickoff rm_watermark"
    try:
        lf = open('{0}'.format(filename), 'wb')
        lf.write(content)
        lf.close()
    except Exception as e:
        log.error('[ERROR] writing of {0}: {1}'.format(filename, e))
    rm_watermark(filename)


def download_worker(outputdir='', szversion='SZ'):
    "download pdfs and write them to files"
    while True:
        # get() waits for urls being added
        page_url = page_url_pool.get()
        if not page_url:
            # all page urls were added to the PagePool, but PagePool is empty
            return
        date_str = extract_date_from_url(page_url)
        pageid_str = extact_pageid_from_url(page_url)
        try:
            pageid_str = pageid_map['S'+pageid_str]
        except KeyError:
            log.warning('could not find page number for pageid {0}'.format(pageid_str))
            pageid_str = 'unmapped_'+pageid_str
        pdf_lnk = 'https://service3.szarchiv.de/hh03/' + str(scrape_iframe_src(page_url))
        pdf_cont = get_content(pdf_lnk)

        # write pdf file
        if len(pageid_str) < 2:
            pageid_str = '{:02}'.format(int(pageid_str))
        filename = '{od}/{szv}_{ds}_{ps}.pdf'.format(szv=szversion,
                                                     od=outputdir,
                                                     ds=date_str,
                                                     ps=pageid_str)
        exec_fileio.submit(fileio_worker, filename, pdf_cont)


def check_file_ex(url):
    "check whether page-url was already downloaded and pdf-file exists"
    outputdir = args.output
    date_str = extract_date_from_url(url)
    pageid_str = extact_pageid_from_url(url)
    try:
        pageid_str = pageid_map['S'+pageid_str]
    except KeyError:
        log.warning('could not find page number for pageid {0}'.format(pageid_str))
        pageid_str = 'unmapped_'+pageid_str
    # write pdf file
    filename = '{od}/{ds}_{ps}.pdf'.format(od=outputdir, ds=date_str, ps=pageid_str)
    if len(glob.glob(filename)) > 0:
        return True
    return False


def handle_day(szversion, day_url):
    global stats_total_pages

    pattern = re.compile(r'hh03.*pagehtm.*SZ\.'+szversion+'\.def\.def.*Z[0-9]+$')
    day_site_urls = scrape_hrefs(day_url)
    get_pageid_mappings(day_url)
    for href in day_site_urls:
        if page_url_pool.poison_stop:
            return
        if pattern.match(href):
            url = 'https://service3.szarchiv.de/hh03/' + href
            if check_file_ex(url):
                m = re.match(r'.*bid=SZ([0-9]*)S([0-9]*)\.SZ\.' + szversion, url)
                log.debug('[handle_day] skipping existing {0}_{1}.pdf'.format(m.group(1), m.group(2)))
                continue
            page_url_pool.add(url)
            stats_total_pages += 1


def download_pattern(szversion, base_url, year, rest=''):
    'download year (YYYY) and rest (MMDD|MM|)'
    year_url = scrape_year_url(szversion, base_url, year)
    days_urls = scrape_day_urls(szversion, year_url, 'SZ.' + szversion +'.'+ year+rest)
    for day_url in days_urls:
        if page_url_pool.poison_stop:
            return
        handle_day(szversion, day_url)

def format_date(date):
    year = '{0}{:02d}{:02d}'.format(date.year)
    month = '{:02d}'.format(date.month)
    day = '{:02d}'.format(date.day)

    return year, month, day

def download_today(szversion, base_url):
    now = datetime.now()
    year, month, day = format_date(now)
    download_pattern(szversion, base_url, year, month+day)


def download_latest(szversion, base_url):
    latest = None

    content = get_content(base_url)
    bs = BeautifulSoup(content, features="html.parser")
    tds = bs.findAll(name="td", attrs={"class":"ET1A"})

    # Find the latest entry in the calendar that still has an href attribute
    for td in tds:
        a = td.find(name="a")
        if a :
            m = re.search(r'bid=navd\.SZ\.SZ\.(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2}).*', a.attrs["href"])
            latest = m.group("year"), m.group("month"), m.group("day")

    log.info("Latest version: {}".format(latest))

    download_pattern(szversion, base_url, latest[0], "".join(latest[1:]))


def setup_logger(log_level=logging.INFO):
    LOGFORMAT = "  %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"
    logging.root.setLevel(log_level)
    formatter = ColoredFormatter(LOGFORMAT)
    stream = logging.StreamHandler()
    stream.setLevel(log_level)
    stream.setFormatter(formatter)
    log = logging.getLogger('pythonConfig')
    log.setLevel(log_level)
    log.addHandler(stream)
    return log


def _args_date_check(date):
    'return YYYYMMDD|YYYYMM|YYYY'
    year = ''
    month = ''
    day = ''
    if len(date) not in [4, 6, 8]:
        return ''
    if len(date) >= 4:
        if int(date[0:4]) >= 1998 and int(date[0:4]) <= 2019:
            year = date[0:4]
    if len(date) >= 6:
        if int(date[4:6]) >= 1 and int(date[4:6]) <= 12:
            month = date[4:6]
    if len(date) >= 8:
        if int(date[6:8]) >= 1 and int(date[6:8]) <= 31:
            day= date[6:8]
    return year + month + day


class FullPaths(argparse.Action):
    "Expand user- and relative-paths"
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, os.path.abspath(os.path.expanduser(values)))


def is_dir(dirname):
    "Checks if a path is an actual directory"
    if not os.path.isdir(dirname):
        msg = "-o: {0}; not a directory".format(dirname)
        raise argparse.ArgumentTypeError(msg)
    else:
        return dirname


def setup_argparse():
    'parse the arguments initially'
    ap = argparse.ArgumentParser()
    date_group = ap.add_mutually_exclusive_group()
    date_group.add_argument('-l', '--latest',
                            dest='latest',
                            help="download only the latest SZ (default)",
                            action='store_true')
    date_group.add_argument('-t', '--today',
                            dest='today',
                            help="download only today's SZ",
                            action='store_true')
    date_group.add_argument('-a', '--all',
                            dest='all',
                            help="download all SZs since 1998",
                            action='store_true')
    date_group.add_argument('-d', '--date',
                            dest='date',
                            help="valid formats: YYYYMMDD|YYYYMM|YYYY (1998-today)",
                            type=str,
                            default=None,
                            metavar='FORMAT')
    ap.add_argument('-szv', '--sz-version',
                    dest='szversion',
                    action='store',
                    choices=['SZ', 'LANGST','JRUE','MAG'],
                    default='SZ',
                    help='SZ: journal, LANGST: langstrecke, JRUE: jahresrueckblick, MAG: magazin')
    ap.add_argument('-v', '--verbose',
                    dest='verbose',
                    help="show verbose debug messages",
                    action="store_true")
    ap.add_argument('-o', '--output',
                    dest='output',
                    help="output directory for saving the pdfs into",
                    metavar='OUTDIR',
                    action=FullPaths,
                    type=is_dir)
    ret = ap.parse_args()
    if ret.date:
        ymd = _args_date_check(ret.date)
        if ymd == '':
            ap.print_help()
            print()
            # dirty hack to disable the printing of a traceback
            sys.tracebacklimit = None
            msg = "--date: {0}; wrong format".format(ret.date)
            raise argparse.ArgumentTypeError(msg)
        ret.date = ymd
    if not ret.output:
        ret.output = os.getcwd()
    return ret


def main():
    global log
    global page_url_pool
    global exec_fileio
    global args
    base_url = None

    # parse the arguments
    args = setup_argparse()
    # configure the logger log
    log_level = logging.DEBUG if args.verbose else logging.INFO
    log = setup_logger(log_level)

    # change cwd for call of bash-script with relative path
    ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
    os.chdir(ROOT_PATH)

    page_url_pool = PagePool()
    max_fileio = 10
    max_download = 22
    exec_fileio = ThreadPoolExecutor(max_workers=max_fileio)
    exec_download = ThreadPoolExecutor(max_workers=max_download)

    # register sigint-handler for controlled thread-exits
    signal.signal(signal.SIGINT, signal_handler_poison)

    print('''
  ************************************************************
  *** welcome to szfetch!
  ***
  *** - if libnet-login continues to fail, check whether you
  ***   can access szarchiv.de manually within the browser:
  ***   https://archiv.szarchiv.de/Portal/restricted/Start.act
  ***
  *** - abort with <Ctrl-c>
  ************************************************************
''')

    log.info('trying to login into szarchive via libnet-login')
    base_url = libnet_login()
    if not base_url:
        log.error('login failed: check your IP address -> libnet')
        sys.exit(1)
    log.info('login attempt succeeded & got base_url')

    if args.szversion != 'SZ':
        base_content = get_content(base_url)
        soup = BeautifulSoup(base_content, features="html.parser")
        base_url = 'https://service3.szarchiv.de:443/hh03/'
        for link in soup.find_all('a'):
            if 'href' in link.attrs:
                if args.szversion == 'LANGST' and 'SZ.LANGST' in link.attrs['href']:
                    base_url += link.attrs['href']
                    break
                elif args.szversion == 'MAG' and 'SZ.MAG' in link.attrs['href']:
                    base_url += link.attrs['href']
                    break
                elif args.szversion == 'JRUE' and 'SZ.JRUE' in link.attrs['href']:
                    base_url += link.attrs['href']
                    break

    # kickoff the downloader-threads
    for _ in range(max_download):
        exec_download.submit(download_worker, args.output, args.szversion)

    if args.date:
        download_pattern(args.szversion, base_url, args.date[:4], args.date[4:])
    elif args.all:
        for year in range(1998, 2019):
            download_pattern(args.szversion, base_url, str(year))
    elif args.today:
        download_today(args.szversion, base_url)
    else:
        download_latest(args.szversion, base_url)
    log.info('{0} urls added to the PagePool'.format(stats_total_pages))
    page_url_pool.set_all_pages_added()

    exec_download.shutdown(wait=True)
    exec_fileio.shutdown(wait=True)


if __name__=='__main__':
    main()
