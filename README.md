# szfetch

Download any issue (since 1998) of the _Sueddeutsche Zeitung_.

Give me feedback if you find any bugs or if you still find
fingerprints/watermarks within the PDFs.


## Changelog

### v0.1

* Download _Sueddeutsche Magazin_, _Suedeutsche Langstrecke_,
  or _Sueddeutsche Jahresrueckblick_ with `--sz-version`.
* Got rid of selenium/phantomjs login

## Usage

```
usage: szfetch.py [-h] [-t | -a | -d FORMAT] [-szv {SZ,LANGST,JRUE,MAG}] [-v]
                  [-o OUTDIR]

optional arguments:
  -h, --help            show this help message and exit
  -t, --today           download only today's SZ (default)
  -a, --all             download all SZs since 1998
  -d FORMAT, --date FORMAT
                        valid formats: YYYYMMDD|YYYYMM|YYYY (1998-today)
  -szv {SZ,LANGST,JRUE,MAG}, --sz-version {SZ,LANGST,JRUE,MAG}
                        SZ: journal, LANGST: langstrecke, JRUE:
                        jahresrueckblick, MAG: magazin
  -v, --verbose         show verbose debug messages
  -o OUTDIR, --output OUTDIR
                        output directory for saving the pdfs into
```

## Dependencies

For `szfetch.py`:

* python3-bs4
* python3-colorlog
* python3-lxml
* python3-requests
* python3-urllib3


Install with:
```
sudo apt-get install python3-bs4 python3-colorlog python3-lxml python3-requests python3-urllib3
```

For `rm_watermark.sh`:

* forensics-extra (exiftool)
* pdftk


Install with:
```
sudo apt-get install forensics-extra pdftk
```

# One big PDF

If you're eager to have one big fat nice PDF instead of X single pages:
```
DAT='SZ_20171013'; pdftk $(ls -v $DAT\_*.pdf) cat output $DAT.pdf && rm $DAT\_*.pdf
```

# asciinema demo (older version of szfetch.py)

[![asciicast](https://asciinema.org/a/haUASmAt8iMGM2eFO5aVmm8nW.png)](https://asciinema.org/a/haUASmAt8iMGM2eFO5aVmm8nW)

# TODOs

* print statistics: show how many files done yet and how many in queue
* merge the single pages to one PDF within `szfetch.py`
* python install scripts instead of manual package manager commands within
  the README.md

# License

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.
