#!/usr/bin/env bash
# kickoff the actual szfetch

BASEDIR_PATH=$(dirname $(readlink -f "$0"))
VENV_PATH=$BASEDIR_PATH/venv
SZDEST_PATH=$BASEDIR_PATH/szs
SZFETCH=$BASEDIR_PATH/szfetch.py
DEV=$BASEDIR_PATH/dev.sh
KICKOFF=$BASEDIR_PATH/kickoff.sh

if [ ! -x "$(command -v pdftk)" ]; then
	echo "[!] pdftk not installed; abort"
	exit 1
fi

if [ ! -d "$SZDEST_PATH" ]; then
	echo "[!] creating SZ directory: $SZDEST_PATH"
	mkdir $SZDEST_PATH;
fi

if [ ! -d "$VENV_PATH" ]; then
	echo "[!] does not exist: $VENV_PATH"
	echo -n "[?] should we install virtual environment (y/n) ? "
	read answer
	if [ "$answer" != "${answer#[Yy]}" ] ;then
		$DEV
		if [ $? -ne 0 ]; then
			echo "[!] installing|sourcing venv failed; abort"
			exit 1
		fi
	else
		exit 1
	fi
fi

. $VENV_PATH/bin/activate
$SZFETCH --latest --output $SZDEST_PATH

# get the base-filename of the current SZ
DAT="$(ls ${SZDEST_PATH} | grep -E 'SZ\_[0-9]{8}\_[0-9]+' | sed 's/\(SZ\_[0-9]\{8\}\)\_[0-9]\+\.pdf/\1/g' | uniq)"
if [ -z "$DAT" ]; then
	# FIXME this whole pattern matching via ls is obviously fucking ugly
	DAT="$(ls ${SZDEST_PATH} | grep -E 'SZ\_[0-9]{8}\_.*' | sed 's/\(SZ\_[0-9]\{8\}\)\_.*\.pdf/\1/g' | uniq)"
fi

# concat all the pages; afterwards: delete the single pages
pdftk $(ls -v ${SZDEST_PATH}/${DAT}_*.pdf) cat output ${SZDEST_PATH}/${DAT}.pdf
if [ $? -ne 0 ]; then
	exit 1
fi
rm -f ${SZDEST_PATH}/${DAT}_*.pdf

# remove all but the 3 recent SZs
for F in $(ls -1 -t $SZDEST_PATH | awk 'NR>3'); do
	rm -f ${SZDEST_PATH}/${F};
done
