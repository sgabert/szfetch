#!/usr/bin/env bash
# remove exif-/metadata and 'watermarks' aka identifying strings

# dependency checks
abort_marker=0
for cmd in "exiftool" "pdftk" "qpdf"; do
	if ! command -v "$cmd" >/dev/null 2>&1 ; then
		echo "[DEPENDENCY] missing: $cmd"
		abort_marker=1
	fi
done
if [ "$abort_marker" -eq 1 ]; then
	exit 1
fi

FILE="${1%.*}"

exit_due_error ()
{
	echo "[$1] an error occured for: $2.pdf"
	exit 1
}

if [[ -f "$1" ]]; then
	# [exiftool] removing exif information
	exiftool -overwrite_original -all:all= ${FILE}.pdf
	if [ $? -ne 0 ]; then exit_due_error "exiftool" "$FILE"; fi

	# [qpdf] decrypt empty password pdfs
	qpdf ${FILE}.pdf ${FILE}.tmp.pdf --decrypt --password=''

	# [pdftk] uncompressing
	pdftk ${FILE}.tmp.pdf output ${FILE}.unc.pdf uncompress &> /dev/null
	if [ $? -ne 0 ]; then exit_due_error "pdftk" "$FILE"; fi

	# [sed] replace identifying strings in uncompressed pdf
	sed -e 's/\/URI\ (.*)/\/URI\ ()/g' \
		-e '/libnetuberlangen/,/\[(SZ[0-9]\{4\}.*)\]TJ/d' \
		-e '/\/Subject/,/\/Producer/d' ${FILE}.unc.pdf > ${FILE}.tmp.pdf

	# [pdftk] recompress altered pdf
	pdftk ${FILE}.tmp.pdf output ${FILE}.pdf compress &> /dev/null
	if [ $? -ne 0 ]; then exit_due_error "pdftk" "$FILE"; fi

	# delete temp files
	rm -f ${FILE}.unc.pdf
	rm -f ${FILE}.tmp.pdf
else
	echo "[${FILE}.pdf] file not found"
	exit 1
fi
