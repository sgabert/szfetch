#!/bin/sh

VENV_PATH=venv

echo "Creating dev environment in $VENV_PATH..."

python3 -m venv $VENV_PATH
. $VENV_PATH/bin/activate
pip3 install -U pip setuptools
pip3 install -r requirements.txt

echo ""
echo "  * Created virtualenv environment in $VENV_PATH."
echo "  * Installed all dependencies into the virtualenv."
echo "  * You can now activate the $(python3 --version) virtualenv with this command: \`. $VENV_PATH/bin/activate\`"
